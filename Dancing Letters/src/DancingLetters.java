/**
* The Dancing Letter program implements an application that
* simply displays string present in inputString variable
* in a dancing pattern.
*
* @author  Abhilash Desai
* @date   2018-02-28 
*/
public class DancingLetters {
	public static void main(String[] args){
		int letterCount =-1;
		String inputString="hi*hello 123#bye"; // Input text
		System.out.print("Result: ");
		for(int i=0;i<inputString.length();i++){
			if(Character.isLetter(inputString.charAt(i))){
				letterCount++;
				if(letterCount%2==0)
					System.out.print(Character.toUpperCase(inputString.charAt(i)));
				else
					System.out.print(Character.toLowerCase(inputString.charAt(i)));
			} else 
				System.out.print(inputString.charAt(i));
			}
		}
	}


